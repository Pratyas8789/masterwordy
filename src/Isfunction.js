
import {init} from './api.js'

let letters = document.getElementsByClassName("box")

let done = false;
let currentletter = '';
let rowindex = 0;
let word
let wordpart

export function addletter(letter) {
    if (currentletter.length < 5) {
        currentletter += letter;
    } else {
        currentletter = currentletter.substring(0, currentletter.length - 1) + letter;
    }
    letters[5 * rowindex + currentletter.length - 1].innerText = letter
}

export async function commit() {
    let word1=await init()
    word=word1.toUpperCase();
    console.log(word);
    wordpart=word.split("")
    const map = makeMap(wordpart);
    if (currentletter.length == 5 && done == false) {

        const guesspart = currentletter.split("");

        for (let i = 0; i < 5; i++) {
            if (guesspart[i] == wordpart[i]) {
                letters[5 * rowindex + i].classList.add("correct");
                map[guesspart[i]]--;
            }
        }
        for (let i = 0; i < 5; i++) {
            if (guesspart[i] == wordpart[i]) {

            } else if (wordpart.includes(guesspart[i]) && map[guesspart[i]] > 0) {
                letters[5 * rowindex + i].classList.add("guess");
                map[guesspart[i]]--;
            } else {
                letters[5 * rowindex + i].classList.add("wrong");
            }
        }
        rowindex++;

        if (currentletter === word) {
            alert('you win')
            done = true;
            return;
        }
        currentletter = '';


        if (rowindex == 6) {
            alert(`you lose this game, the word is ${word}`)
            return;
        }
    }

    function makeMap(array) {
        const obj = {}
        for (let i = 0; i < array.length; i++) {
            const letter = array[i]
            if (obj[letter]) {
                obj[letter]++;
            } else {
                obj[letter] = 1;
            }
        }
        return obj;
    }

}

export function backspace() {
    currentletter = currentletter.substring(0, currentletter.length - 1);
    letters[5 * rowindex + currentletter.length].innerText = ""
}




export function isLetter(letter) {
    return /^[a-zA-Z]$/.test(letter);
}

export function complete () {
    if(done==true)return true;
    return false;
}

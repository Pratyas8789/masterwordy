export async function init(params) {
    const response = await fetch("https://words.dev-apis.com/word-of-the-day");
    const obj = await response.json();
    return obj.word;
}


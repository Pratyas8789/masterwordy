import {addletter, commit, backspace, isLetter, complete } from './Isfunction.js';



if (!navigator.onLine) {
    alert("Sorry your internet is not working!! please try again. ")
} else{
    
    document.addEventListener("keydown", function name(event) {
        const key = event.key
        if (complete() == false) {
            if (key === 'Enter') {
                commit();
            } else if (key === 'Backspace') {
                backspace();
            } else if (isLetter(key)) {
                addletter(key.toUpperCase())
            }
        }
    });
}